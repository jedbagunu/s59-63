import Navbar from 'react-bootstrap/Navbar'
import Nav from 'react-bootstrap/Nav'
import{ Link, NavLink } from 'react-router-dom'
import {useContext} from 'react'
import UserContext from '../UserContext'
import {Container} from 'react-bootstrap';

  

export default function AppNavbar() {


    const {user} = useContext(UserContext)
    return (
    <Navbar style={{backgroundColor: "#071740"}} className="nav fixed-top col-sm-12" expand="lg">
      <Container className="col-8 nav-bg  " >
       <img  src={"https://wlassets.vespa.com/wlassets/vespa/master/logo/Vespa/original/Vespa.png?1592235669985"} alt="Sample Image" height="30"/>
        <Navbar.Brand as={Link} to="/myproducts" className="nav-name" >Motors</Navbar.Brand>
          
        <Navbar.Toggle aria-controls="basic-navbar-nav" />
        <Navbar.Collapse id="basic-navbar-nav">
          <Nav className=" ml-auto">
            <Nav.Link  as={Link} to="/">Home</Nav.Link>
            { (user.isAdmin) ?
                
                <Nav.Link    as={Link} to="/products">Admin Dashboard</Nav.Link>
                 :
                <Nav.Link as={Link} to="/products">Products</Nav.Link>
                  
            }

              <Nav className="ml-auto">
                    {    (user.id !== null) 
                        ?
                            user.isAdmin === true 
                            ?
                              <Nav.Link as={NavLink} to="/logout">Logout</Nav.Link>
                            :
                            <>
                             {/*   <Nav.Link as={NavLink} to="/cart">Cart</Nav.Link>*/}
                              {/*  <Nav.Link as={NavLink} to="/orders">Orders</Nav.Link>*/}
                                <Nav.Link as={NavLink} to="/logout">Logout</Nav.Link>
                            </>
                
              
                        :
                      <>
                      <Nav.Link as={Link} to="/login2">Login</Nav.Link>
                      <Nav.Link as={Link} to="/register">Register</Nav.Link>
                      </>
                    }
            
            </Nav>
          </Nav>
        </Navbar.Collapse>
      </Container>
    </Navbar>
    )
}
