import {Button, Row, Col} from 'react-bootstrap'
import{ Link } from 'react-router-dom'

export default function Banner(){
	return(

		<Row  className=" banner-bg mt-5  ">
			<Col className="py-5 my-5 mt-3"  >
				<h1 className="product-title">Riders Hub</h1>
				<p>Where you can hear the Smooth Clasic Revs</p>
				
				<Link to="/products">
     				 <Button id="submitBtn" className="mt-3" disabled>Let's Ride</Button>
 				</Link>
 				
			 </Col>

		</Row> 
		)
}