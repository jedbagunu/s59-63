import React from 'react';
import img2 from '../images/img2.jpg'
import {  Row, Col } from 'react-bootstrap';

const ProductRow = () => {
  return (
  	<Row>
	  	<Col>
		  	<Row>		
		        <img  src={"https://images.piaggio.com/vespa/vehicles/evpv000vtp/evpvnsnvtp/evpvnsnvtp-04-s.png"} alt="Sample Image" height="200"/>		       
		  	</Row> 
	  	</Col>	
	  	<Col>
	  		<Row>
	  			<Row>
			        <h4>Vespa Sprint</h4>
			        <p>Vespa Sprint S with TFT dashboard is the elevated icon of dynamic and sophisticated design, mixing a fashionable attitude with connectivity features.</p>
	  			</Row> 
	  		</Row>
	  	</Col>
  	</Row>
   

  );
}

export default ProductRow;