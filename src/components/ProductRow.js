import React from 'react';
import img1 from '../images/img1.jpg'
import {  Row, Col } from 'react-bootstrap';
const ProductRow = () => {
  return (
    <Row>
        <Col>
            <Row>       
                <img src={"https://images.piaggio.com/vespa/vehicles/evgs000vte/evgs8znvte/evgs8znvte-04-s.png"} alt="Sample Image" height="200"/>
            </Row> 
        </Col>  
        <Col>
            <Row>
                <Row>
                    <h4>Vespa GTS Super</h4>
                    <p>The icon that enhances style, safely, outstanding sportiness and luxurious comfort</p>
                </Row> 
            </Row>
        </Col>
    </Row>

  );
}

export default ProductRow;