import React, { useEffect, useState } from 'react';
import ProductCard from '../components/ProductCard';
import { Row } from 'react-bootstrap';

export default function CustomerView({productsData}){

	const [products, setProducts] = useState([])

	useEffect(() => {

		const productsArr = productsData.map(productData => {
			if(productData.isActive === true){
				return <ProductCard data={productData} key={productData.productId} breakPoint={4}/>
			}else{
				return null
			}
		})

		setProducts(productsArr)
	}, [productsData])

	return(
		<React.Fragment>
			<h2 className="product-title text-center mt-5">Motorcycle Line-up</h2>
			<Row>
				{products}
				
			</Row>
		</React.Fragment>
	)
}