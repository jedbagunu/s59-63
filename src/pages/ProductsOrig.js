import ProductCard from '../components/ProductCard'

import {useEffect, useState} from 'react'
import Loading from '../components/Loading'


export default function Product(){
  const [products, setproducts] = useState([])
  const [isLoading, setIsLoading] = useState(false)


  useEffect((isLoading) => {
    //Set loading state to true
      setIsLoading(true)

      fetch(`${process.env.REACT_APP_API_URL}/products`)
      .then(response => response.json())
      .then(result => {

          setproducts(
              result.map(product => {
               return (
                 <ProductCard key={product._id} product = {product}/>
                    )
               })

            )
          //Sets the loading to false
          setIsLoading(false)
      })

  }, [])

  return ( 
     (isLoading) ?
          <Loading/>
        :   
          <>
             {products}

          </>

   ) 
    
}

