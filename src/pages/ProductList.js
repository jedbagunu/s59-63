import React, { Component } from 'react';
import ProductRow from '../components/ProductRow';
import ProductRow2 from '../components/ProductRow2';
import {Col} from 'react-bootstrap'


class ProductList extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <Col> 
        <ProductRow />
       <ProductRow2 />
      </Col>
    );
  }
}

export default ProductList;