/*import React, { useState, useEffect } from 'react';
import { Container, InputGroup, Button, FormControl, Table } from 'react-bootstrap'
import { Link, Navigate } from 'react-router-dom';
import { useContext } from 'react';

import {useParams, useNavigate} from 'react-router-dom'
import UserContext from '../UserContext'

import Swal from 'sweetalert2'



	// Gets the productId from the URL of the route that this component is connected to. '/products/:productId'
	
export default function MyCart(){
	const {productId} = useParams()

	const {user} = useContext(UserContext)

	const navigate = useNavigate()

	const [name, setName] = useState("");
	const [description, setDescription] = useState("");
	const [price, setPrice] = useState(0);
	const [total, setTotal] = useState(0)
	const [cart, setCart] = useState([])
	const [tableRows, setTableRows] = useState([])
	const [willNavigate, setWillNavigate] = useState(false)

	useEffect(()=> {
		if(localStorage.getItem('cart')){
			setCart(JSON.parse(localStorage.getItem('cart')))
		}
	}, [])

	useEffect(()=> {

		let cartItems = cart.map((item, index) => {

			return (
				<Container >
					 <tr  key={item.productId}>
		          <td><Link to={`/products/${item.productId}`}>{item.name}</Link></td>
		          <td>₱{item.price}</td>
		          <td>
				  	<InputGroup className="d-md-none">
						<FormControl type="number" min="1" value={item.quantity} onChange={e => qtyInput(item.productId, e.target.value)}/>
					</InputGroup>
					<InputGroup className="d-none d-md-flex w-50">
						<InputGroup.Prepend>
							<Button variant="secondary" onClick={() => qtyBtns(item.productId, "-")}>-</Button>
						</InputGroup.Prepend>
						<FormControl type="number" min="1" value={item.quantity} onChange={e => qtyInput(item.productId, e.target.value)}/>
						<InputGroup.Append>
							<Button variant="secondary" onClick={() => qtyBtns(item.productId, "+")}>+</Button>
						</InputGroup.Append>
					</InputGroup>
		          </td>
		          <td>₱{item.subtotal}</td>
		          <td className="text-center">
		          	<Button variant="danger" onClick={() => removeBtn(item.productId)}>Remove</Button>
		          </td>
		      </tr>			
				</Container>
		     
			)
		})
		setTableRows(cartItems)

		let tempTotal = 0

		cart.forEach((item)=> {
			tempTotal += item.subtotal
		})

		setTotal(tempTotal)
	}, [cart])

	const qtyInput = (productId, value) => {

		let tempCart = [...cart]

		if(value === ''){
			value = 1
		}else if(value === "0"){
			alert("Quantity can't be lower than 1.")
			value = 1
		}

		for(let i = 0; i < cart.length; i++){

			if(tempCart[i].productId === productId){
				tempCart[i].quantity = parseFloat(value)
				tempCart[i].subtotal = tempCart[i].price * tempCart[i].quantity
			}
		}

		setCart(tempCart)
		localStorage.setItem('cart', JSON.stringify(tempCart))	
	}

	const qtyBtns = (productId, operator) => {

		let tempCart = [...cart]

		for(let i = 0; i < tempCart.length; i++){

			if(tempCart[i].productId === productId){
				if(operator === "+"){
					tempCart[i].quantity += 1
					tempCart[i].subtotal = tempCart[i].price * tempCart[i].quantity
				}else if(operator === "-"){
					if(tempCart[i].quantity <= 1){
						alert("Quantity can't be lower than 1.")
					}else{
						tempCart[i].quantity -= 1
						tempCart[i].subtotal = tempCart[i].price * tempCart[i].quantity
					}
				}
			}
		}

		setCart(tempCart)
		localStorage.setItem('cart', JSON.stringify(tempCart))
	}

	const removeBtn = (productId) => {
		let tempCart = [...cart]

		let cartIds = tempCart.map((item)=> {
			return item.productId
		})

		tempCart.splice([cartIds.indexOf(productId)], 1)

		setCart(tempCart)
		localStorage.setItem('cart', JSON.stringify(tempCart))	
	}

	const checkout = () => {

		const checkoutCart =  cart.map((item) => {
			return {
				productName: item.name,
				productId: item.productId,
				quantity: item.quantity
			}
		})

		fetch(`${process.env.REACT_APP_API_URL}/users/create`, {
			method: 'POST',
			headers: {
				"Content-Type": "application/json",
				Authorization: `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				productId:productId,
				userId: user.id
			})
		})
		.then(response => response.json())
		.then(result => {
			if(result) {
				Swal.fire({
					title: "Success!",
					icon: "success",
					text: "You have ordered successfully!"
				})

				navigate('/products')
			} else {
				console.log(result)

				Swal.fire({
					title: "Something went wrong!",
					icon: "error",
					text: "Please try again :("
				})
			}
		})
	
	}

	return(
		willNavigate === true
		? <Navigate to="/orders"/>
		:
		cart.length > 0
			? <Container>
				<h3 className="cart text-center">Your cart is empty! <Link to="/products">Start shopping.</Link></h3>
			</Container>
			:
			<Container className='cart1 py-5 mt-3'>
				<h2 >Your Shopping Cart</h2>
				<Table striped bordered hover responsive>
					<thead className="bg-secondary text-white">
						<tr>
							<th>Name</th>
							<th>Price</th>
							<th>Quantity</th>
							<th>Subtotal</th>
							<th></th>
						</tr>
					</thead>
					<tbody>
						{tableRows}
						<tr>
							<td colSpan="3">
								<Button variant="success" block onClick={()=> checkout()}>Checkout</Button>
							</td>
							<td colSpan="2">
								<h3>Total: ₱{total}</h3>
							</td>
						</tr>
					</tbody>						
				</Table>
			</Container>
	)
}*/