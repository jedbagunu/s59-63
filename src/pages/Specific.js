import { useState, useEffect, useContext } from 'react';
import { Container, Card, Button, Row, Col } from 'react-bootstrap';
import {useParams, useNavigate, Link} from 'react-router-dom'
import UserContext from '../UserContext'

import Swal from 'sweetalert2'

export default function ProductView() {

	// Gets the productId from the URL of the route that this component is connected to. '/products/:productId'
	const {productId} = useParams()

	const {user} = useContext(UserContext)

	const navigate = useNavigate()

	const [name, setName] = useState("");
	const [description, setDescription] = useState("");
	const [price, setPrice] = useState(0);

	const createOrder = (productId) => {
		fetch(`${process.env.REACT_APP_API_URL}/users/create`, {
			method: 'POST',
			headers: {
				"Content-Type": "application/json",
				Authorization: `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				productId:productId,
				userId: user.id
			})
		})
		.then(response => response.json())
		.then(result => {
			if(result) {
				Swal.fire({
					title: "Success!",
					icon: "success",
					text: "You have ordered successfully!"
				})

				navigate('/products')
			} else {
				console.log(result)

				Swal.fire({
					title: "Something went wrong!",
					icon: "error",
					text: "Please try again :("
				})
			}
		})
	
	}

	useEffect(() => {
		fetch(`${process.env.REACT_APP_API_URL}/products/${productId}/single`)
		.then(response => response.json())
		.then(result => {
			setName(result.name)
			setDescription(result.description)
			setPrice(result.price)
		})
	}, [productId])

	return(
		<Container className="mt-5 p-3 ">
			<Row>
				<Col lg={{ span: 12}}>
					<Card className="bg-specific">
						<Card.Body className="text-center bg-specific mt-5">
							<Card.Title>{name}</Card.Title>
							<Card.Subtitle>Description:</Card.Subtitle>
							<Card.Text>{description}</Card.Text>
							<Card.Subtitle>Price:</Card.Subtitle>
							<Card.Text>PhP {price}</Card.Text>
							<Card.Subtitle>Available</Card.Subtitle>
							<Card.Text>Original QC passed</Card.Text>
							{	user.id !== null && user.isAdmin === false ? 

									<Button variant="primary" onClick={() => createOrder(productId)}>Buy now | Checkout</Button>
								:
									<Link className="btn btn-danger btn-block" to="/login2">Please Login as a User</Link>
							}
							
						</Card.Body>		
					</Card>
				</Col>
			</Row>
				
		</Container>


	)
}
