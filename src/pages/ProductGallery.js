import React from 'react';
import '../App.css';
import ProductList from './ProductList';
import {Button, Row, Col} from 'react-bootstrap'
import{ Link } from 'react-router-dom'

function ProductGallery() {
  return (
    <div className="myProduct mt-4 py-4 ">
      <header className="header">
        <h1 >My Products</h1>
        <Link to="/products">
             <Button id="submitBtn" className="mt-3" disabled>Let's Ride</Button>
        </Link>
      </header>
      <ProductList/>
    </div>
  );
}

export default ProductGallery