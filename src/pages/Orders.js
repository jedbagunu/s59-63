// import React, { useState, useEffect,  useContext } from 'react';
// import { Container, Card, Accordion } from 'react-bootstrap'
// import { Link } from 'react-router-dom'

// import UserContext from '../UserContext';

// export default function Orders(){

// 	 const [ordersList, setOrdersList] = useState([])
// 	 const { user } = useContext(UserContext)

// 	useEffect(()=> {

// 		fetch(`${ process.env.REACT_APP_API_URL}/users/allOrders`, {
// 			headers: {
// 				'Content-Type': 'application/json'
				
// 			}
// 		})
// 		.then(res => res.json())
// 		.then(data => {
// 			if(data.length > 0){
// 				let orders = data.map((item, index)=> {
// 					return(
// 						<Card key={item._id}>
// 							<Accordion.Toggle as={Card.Header} eventKey={index + 1} className="bg-secondary text-white">
// 								Order #{index + 1} - Purchased on: {(item.purchasedOn).format("MM-DD-YYYY")} (Click for Details)
// 							</Accordion.Toggle>
// 							<Accordion.Collapse eventKey={index + 1}>
// 								<Card.Body>
// 									<h6>Items:</h6>
// 									<ul>
// 									{
// 										item.user.map((order) => {
// 											return (
// 												<li key={order.productId}>{order.name} - Quantity: {order.quantity}</li>
// 											)
// 										})
// 									}
// 									</ul>
// 									<h6>Total: <span className="text-warning">₱{item.totalAmount}</span></h6>
// 								</Card.Body>
// 							</Accordion.Collapse>
// 						</Card>
// 					)
// 				})
	
// 				setOrdersList(orders)				
// 			}
// 		})
// 	}, [])

// 	return(
// 		ordersList.length > 0
// 		?<Container className="text-center py-5 mt-4">
		
// 				<h3 className="text-center py-5 mt-4">No orders placed yet! <Link to="/products">Start shopping.</Link></h3>
// 		</Container>
// 		:
// 		<Container>
// 			<h2 className="text-center py-5 mt-4">Order History</h2>
// 			<Accordion>
// 				{ordersList}
// 			</Accordion>
// 		</Container>
// 	)
// }