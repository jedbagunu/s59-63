import './App.css';

import {useState} from 'react'
import {UserProvider} from './UserContext'
import Banner from './components/Banner'
import AppNavbar from './components/AppNavbar'
import ProductRow from './components/ProductRow'
import CustomerView from './components/CustomerView'
import Home from './pages/Home'
import Products from './pages/Products'
import AdminView from './components/AdminView'
import Register from './pages/Register'
import Login2 from './pages/Login2'
import Logout from './pages/Logout'
import ErrorPage from './pages/ErrorPage'
import Specific from './pages/Specific'
// import Cart from './pages/Cart'
import Orders from './pages/Orders'
import MyProducts from './components/MyProducts'


import { Container } from 'react-bootstrap'
import { BrowserRouter as Router, Route, Routes }from 'react-router-dom'


function App() {
  const [user, setUser] = useState({
    id: null,
    isAdmin: null
  })

    const unsetUser = () => {
      localStorage.clear()
    }
  return (
  <>
      {/*Provides the user context throughout any component inside of it*/}
      <UserProvider value={{user, setUser, unsetUser}}>
          {/* Initialize route switching*/}
          <Router>
          <AppNavbar/>
          <Container > 
              <Routes> 
                <Route  path= "/" element={<Home/>}/>
                <Route  path= "/myproducts" element={<MyProducts/>}/>
                <Route  path= "/productrow" element={<ProductRow/>}/>
                <Route  path= "/adminview" element={<AdminView/>}/>
                <Route  path= "/products" element={<Products/>}/>
                <Route  path= "/products/:productId" element={<Specific/>}/> 
                {/*<Route  path="/cart" element={<Cart/>}/>*/}
           {/*     <Route  path="/orders" element={<Orders/>}/>*/}
                <Route  path= "/login2" element={<Login2/>}/>
                <Route  path= "/register" element={<Register/>}/>
                <Route  path= "/logout" element={<Logout/>}/>
                <Route  path="*" element={<ErrorPage/>}/> 


   
              </Routes>
          </Container> 
          </Router>
      </UserProvider>
  </>

  )
}

export default App;
